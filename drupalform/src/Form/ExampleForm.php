<?php

/**
 * @file
 * Contains \Drupal\drupalform\Form\ExampleForm.
 */

namespace Drupal\drupalform\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\Plugin\Mail\PhpMail;
use Exception;

class ExampleForm extends FormBase
{
    public const FROM = 'Example.Form@gmail.com';
    public const HUBSPOT_URL = "https://api.hubapi.com/contacts/v1/contact/createOrUpdate/email/";
    public const HUBSPOT_KEY = '/?hapikey=919cbb34-f700-4820-bbb8-329b6720ac11';

    /**
     * Returns a unique string identifying the form.
     *
     * The returned ID should be a unique string that can be a valid PHP function
     * name, since it's used in hook implementation names such as
     * hook_form_FORM_ID_alter().
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'drupalform_example_form';
    }

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state): array
    {
        $form['firstName'] = [
            '#type' => 'textfield',
            '#title' => 'First Name',
        ];

        $form['lastName'] = [
            '#type' => 'textfield',
            '#title' => 'Last Name',
        ];

        $form['subject'] = [
            '#type' => 'textfield',
            '#title' => 'Subject',
        ];

        $form['message'] = [
            '#type' => 'textarea',
            '#title' => 'Message',
        ];

        $form['email'] = [
            '#type' => 'email',
            '#title' => 'E-mail',
        ];


        $form['submit'] = array
        (
            '#type' => 'submit',
            '#value' => 'SEND',
        );

        return $form;
    }

    /**
     * Form submission handler.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state): void
    {

        try {
            $firstName = $form_state->getValue('firstName');
            $lastName = $form_state->getValue('lastName');
            $subject = $form_state->getValue('subject');
            $body = $form_state->getValue('message');
            $to = $form_state->getValue('email');

            $mistakes = $this->manualValidation($firstName, $lastName, $subject, $body, $to);

            if ($mistakes['status']) {
                $this->addError($mistakes['message'] ?? 'check the correctness of filling out the form');
                return;
            }
            $addContactsResult = $this->createHubSpotAccount($firstName, $lastName, $to);

            if (!$addContactsResult) {
                $this->addError('Not possible to create a contact. See error log');
                return;
            }

            $mailResult = $this->sendMail($to, $subject, $body);

            if ($mailResult) {
                Drupal::logger('drupalform')->notice("status :: The letter was sent , email :: $to , subject :: $subject , content :: $body");
                $this->addStatus('The letter was sent');
            } else {
                $this->addError('Having trouble sending email');
            }
        } catch (Exception $e) {
            Drupal::logger('drupalform')->critical('ERROR :: submitForm() => ' . $e->getMessage());
            $this->addError('Not possible to process the request. See error log.');

        }

    }

    /**
     * This function create new contact for account on hubspot.com
     *
     * @param $firstName
     * @param $lastName
     * @param $email
     * @return bool
     */
    private function createHubSpotAccount($firstName, $lastName, $email): bool
    {

        try {

            $url = self::HUBSPOT_URL . $email . self::HUBSPOT_KEY;

            $data = array(
                'properties' => [
                    [
                        'property' => 'firstname',
                        'value' => $firstName
                    ],
                    [
                        'property' => 'lastname',
                        'value' => $lastName
                    ]
                ]
            );

            $json = json_encode($data, true);

//          $response =
            Drupal::httpClient()->post($url . '&_format=hal_json', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body' => $json
            ]);

            $this->addStatus("$firstName $lastName has been added to your contacts");
            return true;

        } catch (Exception $e) {
            Drupal::logger('drupalform')->critical('ERROR :: createHubSpotAccount() => ' . $e->getMessage());
            return false;
        }
    }


    /**
     * This function sends an email to the specified address.
     *
     * @param $to
     * @param $subject
     * @param $body
     * @return bool
     */
    private function sendMail($to, $subject, $body)
    {
        $message['headers'] = [
            'content-type' => 'text/html',
            'MIME-Version' => '1.0',
            'reply-to' => self::FROM,
            'from' => self::FROM
        ];

        $message['to'] = $to;
        $message['subject'] = $subject;
        $message['body'] = $body;
        /** @var PhpMail $send_mail */
        $send_mail = new PhpMail();
        $result = $send_mail->mail($message);
        return $result;

    }

    /**
     * {@inheritDoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
//        parent::validateForm($form, $form_state); // TODO: Change the autogenerated stub
    }

    /**
     * This method check params from form
     *
     * @param $firstName
     * @param $lastName
     * @param $subject
     * @param $body
     * @param $to
     * @return array
     */
    private function manualValidation($firstName, $lastName, $subject, $body, $to): array
    {
        // CHECK FIRST NAME

        if ($firstName === '') {
            return $this->rejectFormFieldsContent('Not filled field First Name');
        }

        // CHECK LAST NAME

        if ($lastName === '') {
            return $this->rejectFormFieldsContent('Not filled field Last Name');
        }

        // CHECK SUBJECT

        if ($subject === '') {
            return $this->rejectFormFieldsContent('Not filled field Subject');
        }

        // CHECK BODY

        if ($body === '') {
            return $this->rejectFormFieldsContent('Not filled field Message');
        }

        // CHECK EMAIL TO

        if ($to === '') {
            return $this->rejectFormFieldsContent('Not filled field Email');
        }

        if (strpos($to, '@') === false) {
            return $this->rejectFormFieldsContent('Email must contain "@" character');
        }

        $emailFirstSplit = explode('@', $to);

        if ($emailFirstSplit[0] === '') {
            return $this->rejectFormFieldsContent('Forgot something before "@"');
        }

        $secondPart = $emailFirstSplit[1];

        if ($secondPart === '') {
            return $this->rejectFormFieldsContent('Forgot something after "@"');
        }

        if (strpos($secondPart, '.') === false) {
            return $this->rejectFormFieldsContent('Missing part of the email after the "@"');
        }

        $emailSecondSplit = explode('.', $secondPart);

        if ($emailSecondSplit[0] === '') {
            return $this->rejectFormFieldsContent('Something is lost before "."');
        }

        if ($emailSecondSplit[1] === '') {
            return $this->rejectFormFieldsContent('Something is lost after "."');
        }

        return ['status' => false];

    }

    /**
     * This function create reject object for validate form fields
     *
     * @param $message
     * @return array
     */
    private function rejectFormFieldsContent($message)
    {
        return [
            'status' => true,
            'message' => $message
        ];
    }


    /**
     * This function call messenger and add status message to page
     *
     * @param $message
     */
    private function addStatus($message): void
    {
        $this->messenger()->addStatus($message);
    }

    /**
     * This function call messenger and add error message to page
     *
     * @param $message
     */
    private function addError($message): void
    {
        $this->messenger()->addError($message);
    }
}
